import { Component, OnInit } from '@angular/core';
import { Product } from './../../interfaces/product.model';
import { ProductService } from 'src/app/services/products/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  /** VARIABLE PARA TODOS LOS PRODUCTOS */
  products: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productService.getAllProducts().subscribe(
      products => {
        this.products = products;
        console.log(products);
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
// Servicio de productos
import { Product } from './../../interfaces/product.model';
import { ProductService } from 'src/app/services/products/product.service';
import { CartService } from 'src/app/core/services/cart.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: Product;

  constructor(
    private activeRoute: ActivatedRoute,
    private productService: ProductService,
    private cart: CartService
  ) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(
      (params: Params) => {
        const id = params.id;
        this.productService.getProduct(id).subscribe(
          response => {
            this.product = response;
          }
        );
        console.log(this.product);
      }
    );
  }

  createProduct() {
    const newProduct: Product = {
      id: '12',
      title: 'Mi producto',
      image: 'assets/images/mug.png',
      price: 1200,
      description: 'Description'
    };

    this.productService.createProduct(newProduct).subscribe(
      product => {
        console.log(product);
      }
    )
  }

  updateProduct() {
    const newProduct: Partial<Product> = {
      title: 'Mi producto editado',
      price: 3000,
      description: 'Description editada'
    };

    this.productService.updateProduct('12', newProduct).subscribe(
      product => {
        console.log(product);
      }
    );
  }

  deleteProduct(id: string) {
    this.productService.deleteProduct(id).subscribe(
      product => {
        console.log(product);
      }
    );
  }

  addCart() {
    this.cart.addCart(this.product);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyVlidators } from 'src/app/utils/validators';
import { ProductService } from 'src/app/services/products/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/products/interfaces/product.model';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
})
export class EditProductComponent implements OnInit {

  form: FormGroup;
  productId: string;
  product: Product;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.formBuild();
    // this.actRoute.params.subscribe(
    //   params => {
    //     this.productId = params.id;
    //   },
    // )
  }

  ngOnInit() {
    this.actRoute.params.subscribe(
      params => {
        this.productId = params.id;
        this.productService.getProduct(this.productId).subscribe(
          (product: Product) => {
            console.log(product);
            this.form.patchValue(product);
          }
        );
      },
    );
  }

  private formBuild() {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      price: [0, [Validators.required, MyVlidators.isPriceValid]],
      image: [''],
      description: ['', [Validators.required]],
    })
  }

  editProduct(event: Event) {
    event.preventDefault();
    console.log(this.form.value, this.productId);

    if (this.form.valid) {
      const product = this.form.value;
      this.productService.updateProduct(this.productId, product).subscribe(
        newProduct => {
          console.log(newProduct);
          this.router.navigate(['/admin/products']);
        }
      )
    }

  }

}

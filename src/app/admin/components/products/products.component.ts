import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/products/product.service';
import { Product } from 'src/app/products/interfaces/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: []
})
export class ProductsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'title', 'precio', 'actions'];

  products: Product[] = [];

  constructor(private productService: ProductService) { }

  ngOnInit() {

    this.productService.getAllProducts().subscribe(
      (products) => {
        this.products = products;
      }
    );
  }

  deleteProduct(id: string) {
    this.productService.deleteProduct(id).subscribe(
      product => {
        if (product) {
          this.getAllProducts();
        }
      }
    )
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(
      (products) => {
        this.products = products;
      }
    );
  }

}

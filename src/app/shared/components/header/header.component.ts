import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/core/services/cart.service';
import { Product } from 'src/app/products/interfaces/product.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  total$: Observable<number>;

  constructor(private cart: CartService) {
    this.total$ = this.cart.cart$.pipe(
      map(products => products.length)
    );
    // .subscribe(
    //   total => {
    //     this.products = total;
    //   }
    // );
  }

  ngOnInit() {
  }



}

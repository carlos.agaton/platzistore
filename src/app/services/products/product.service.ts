import { Injectable } from '@angular/core';
import { Product } from 'src/app/products/interfaces/product.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {
    console.log(environment.url_api);
  }

  getAllProducts() {
    return this.http.get<Product[]>(environment.url_api + 'products');
  }

  getProduct(id: string) {
    // return this.products.find(item => id === item.id);
    return this.http.get<Product>(environment.url_api + 'products/' + id);
  }

  createProduct(product: Product) {
    return this.http.post(`${environment.url_api}products`, product);
  }

  updateProduct(id: string, changes: Partial<Product>) {
    return this.http.put(`${environment.url_api}products/${id}`, changes);
  }

  deleteProduct(id: string) {
    return this.http.delete(`${environment.url_api}products/${id}`);
  }

}

import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "platzi-store";

  power: Number = 2;

  products = [
    {
      id: 5,
      image: 'assets/images/camiseta.png',
      title: 'Title',
      description: "lorem lorem"
    }
  ];

  clickProduct(id: number) {
    console.log("Product: " + id);
  }
}
